from odoo import models, fields, api, _

import logging
_logger = logging.getLogger(__name__)

class PosOrder(models.Model):
    _inherit = 'pos.order'

    livreur_id = fields.Many2one('hr.employee', string='Livreur', readonly=True)
    livraison = fields.Boolean('Livraison', readonly=True, default=0)
    domicile = fields.Boolean('Domicile', readonly=True, default=0)
    amount_total = fields.Float(compute='_compute_amount_all', string='Total', digits=0 ,store=True)
    @api.model
    def _order_fields(self, ui_order):
        order_fields = super(PosOrder, self)._order_fields(ui_order)
        order_fields['livreur_id'] = ui_order.get('livreur_id', False)
        order_fields['livraison'] = ui_order.get('livraison', 0)
        order_fields['domicile'] = ui_order.get('domicile', 0)
        return order_fields

