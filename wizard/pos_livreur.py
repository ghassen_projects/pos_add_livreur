from odoo import api, fields, models

class PosLivreurs(models.TransientModel):
    _name = 'pos.livreur.wizard'
    _description = 'Livreurs Report'

    start_date = fields.Datetime(required=True, default=fields.Datetime.now)
    end_date = fields.Datetime(required=True, default=fields.Datetime.now)
    pos_livreurs_id = fields.Many2one('hr.employee', domain=[("is_livreur", "=", True)])

    @api.onchange('start_date')
    def _onchange_start_date(self):
        if self.start_date and self.end_date and self.end_date < self.start_date:
            self.end_date = self.start_date

    @api.onchange('end_date')
    def _onchange_end_date(self):
        if self.end_date and self.end_date < self.start_date:
            self.start_date = self.end_date

    @api.multi
    def generate_report(self):
        print ' **** generate_report ****'
        data = {'date_start': self.start_date, 'date_stop': self.end_date}
        data.update(self.env['report.pos_add_livreur.report_livreurs'].get_livreurs(
            self.start_date, self.end_date, self.pos_livreurs_id))
        return self.env['report'].get_action(
            [], 'pos_add_livreur.report_livreurs', data=data)
