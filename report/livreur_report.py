from odoo import models, fields, api
from datetime import timedelta

import logging
_logger = logging.getLogger(__name__)


class ReportLivreurs(models.AbstractModel):

    _name = 'report.pos_add_livreur.report_livreurs'


    @api.model
    def get_livreurs(self, date_start=False, date_stop=False, pos_livreurs_id=False):

        today = fields.Datetime.from_string(fields.Date.context_today(self))
        if date_start:
            date_start = fields.Datetime.from_string(date_start)
        else:
            # start by default today 00:00:00
            date_start = today

        if date_stop:
            # set time to 23:59:59
            date_stop = fields.Datetime.from_string(date_stop)
        else:
            # stop by default today 23:59:59
            date_stop = today + timedelta(days=1, seconds=-1)

        # avoid a date_stop smaller than date_start
        date_stop = max(date_stop, date_start)

        date_start = fields.Datetime.to_string(date_start)
        date_stop = fields.Datetime.to_string(date_stop)

        livreur = self.env['hr.employee'].search([
            ('id', '=', pos_livreurs_id.id)
        ])


        list_livreurs = []
        if livreur:
            orders = self.env['pos.order'].search([
                ('date_order', '>=', date_start),
                ('date_order', '<=', date_stop),
                ('state', 'in', ['paid','invoiced','done']),
                ('livreur_id', '=', pos_livreurs_id.id)])

            nb_livraisons = 0
            total = 0.0
            for order in orders:
                nb_livraisons += 1
                total += order.amount_total
            list_livreurs.append({'livreur' : livreur.name_related,'nb':nb_livraisons,'total':total})
        else:
            orders = self.env['pos.order'].search([
                ('date_order', '>=', date_start),
                ('date_order', '<=', date_stop),
                ('state', 'in', ['paid', 'invoiced', 'done'])])
            list = []
            for order in orders:
                cur_livreur = order.livreur_id
                if cur_livreur not in list:
                    if cur_livreur.name_related:
                        print 'livreur = ',cur_livreur.name_related
                        list.append(cur_livreur)
                        nb_livraisons = 0
                        total = 0.0
                        for order1 in orders:
                            if cur_livreur == order1.livreur_id:
                                nb_livraisons += 1
                                total += order1.amount_total
                        list_livreurs.append({'livreur': cur_livreur.name_related, 'nb': nb_livraisons, 'total': total})


        return {
            'date_start': date_start,
            'date_stop': date_stop,
            'list_livreurs': list_livreurs,
        }

    @api.model
    def render_html(self, docids, data=None):
        return self.env['report'].render('pos_add_livreur.report_livreurs', data)
