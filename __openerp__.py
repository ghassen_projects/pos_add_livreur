{
    'name' : 'Ajouter Livreur',
     'author' : 'GHASSEN TIMOUMI',
    'category' : 'Point Of Sale',
    'version' : '1',
    'summary': 'Payment to the end',
    'description': """ Payment to the end """,

    'depends': [
        'point_of_sale',
        'pos_add_client_ticket',
        'hr_add_to_pos',
        'hr',
    ],
    'data': [
	    'views/backend.xml',
        'view/point_of_sale_view.xml',
        'view/report_livreurs.xml',
        'report/pos_order_report_view.xml',
        'wizard/pos_livreur.xml',

    ],
   
    'qweb': ['static/src/xml/pos.xml'],
    'installable': True,
}
