odoo.define('pos_add_livreur.screens', function (require) {
"use strict";

var gui = require('point_of_sale.gui');
var core = require('web.core');
var QWeb = core.qweb;
var _t = core._t;
var Model = require('web.DataModel');
var screens = require('point_of_sale.screens');

    var ActionpadWidget = screens.ActionpadWidget.include({
    renderElement: function() {
        var self = this;
        this._super();
        var order = self.pos.get_order();
        this.$('.pay').click(function(){
            var has_valid_product_lot = _.every(order.orderlines.models, function(line){
                return line.has_valid_product_lot();
            });
            if(!has_valid_product_lot){
                self.gui.show_popup('confirm',{
                    'title': _t('Empty Serial/Lot Number'),
                    'body':  _t('One or more product(s) required serial/lot number.'),
                    confirm: function(){
                        self.gui.show_screen('payment');
                    },
                });
            }else{
                self.gui.show_screen('payment');
            }
        });
        this.$('.set-customer').click(function(){
            self.$('#selection_livreur').show();
            if ( order.get_domicile() && order.get_client() ){
                order.set_client(null);
            }
            self.gui.show_screen('clientlist');
            order.set_domicile(0);
            order.set_livraison(1);
        });
        this.$('#livraison').click(function(){
            self.$('#selection_livreur').hide();
            if ( order.get_livraison() && order.get_client() ){
                order.set_client(null);
            }
            self.gui.show_screen('clientlist');
            order.set_livraison(0);
            order.set_domicile(1);
        });
        },
    });

    var PaymentScreenWidget = screens.PaymentScreenWidget.include({
    init: function(parent, options) {
        var self = this;
        this._super(parent, options);
        this.livreurs = this.pos.livreurs;
    },
    order_changes: function(){
        var self = this;
        var order = this.pos.get_order();
        if (!order) {
            return;
        } else if (order.is_paid()) {
            console.log('order.is_paid()')
            self.$('.next').addClass('highlight');
        }else{
            console.log('!order.is_paid()')
            self.$('.next').removeClass('highlight');
        }

        if (order.get_livraison() && order.get_client()){
            this.$('#livreur_container').show();
            this.$('#selection_livreur').on('change', function(e){
                var fields;
                var livreur = self.$( "#selection_livreur option:selected" ).text();

                for(var i=0; i < self.livreurs.length; i++){
                    if (self.livreurs[i].name == livreur){
                        fields = self.livreurs[i];
                    }
                }
                self.pos.get_order().set_selected_livreur(fields);
            });
        }else{
            this.$('#livreur_container').hide();
        }
    },
    validate_order: function(force_validation) {
        var self = this;
        var order = this.pos.get_order();

        if (order.get_orderlines().length === 0) {
            this.gui.show_popup('error',{
                'title': _t('Empty Order'),
                'body':  _t('There must be at least one product in your order before it can be validated'),
            });
            return;
        }

        var plines = order.get_paymentlines();
        for (var i = 0; i < plines.length; i++) {
            if (plines[i].get_type() === 'bank' && plines[i].get_amount() < 0) {
                this.pos_widget.screen_selector.show_popup('error',{
                    'message': _t('Negative Bank Payment'),
                    'comment': _t('You cannot have a negative amount in a Bank payment. Use a cash payment method to return money to the customer.'),
                });
                return;
            }
        }

        if (!order.is_paid() || this.invoicing) {
            return;
        }
        if (Math.abs(order.get_total_with_tax() - order.get_total_paid()) > 0.00001) {
            var cash = false;
            for (var i = 0; i < this.pos.cashregisters.length; i++) {
                cash = cash || (this.pos.cashregisters[i].journal.type === 'cash');
            }
            if (!cash) {
                this.gui.show_popup('error',{
                    title: _t('Cannot return change without a cash payment method'),
                    body:  _t('There is no cash payment method available in this point of sale to handle the change.\n\n Please pay the exact amount or add a cash payment method in the point of sale configuration'),
                });
                return;
            }
        }

        if (!force_validation && (order.get_total_with_tax() * 1000 < order.get_total_paid())) {
            this.gui.show_popup('confirm',{
                title: _t('Please Confirm Large Amount'),
                body:  _t('Are you sure that the customer wants to  pay') +
                       ' ' +
                       this.format_currency(order.get_total_paid()) +
                       ' ' +
                       _t('for an order of') +
                       ' ' +
                       this.format_currency(order.get_total_with_tax()) +
                       ' ' +
                       _t('? Clicking "Confirm" will validate the payment.'),
                confirm: function() {
                    self.validate_order('confirm');
                },
            });
            return;
        }

        if (order.is_paid_with_cash() && this.pos.config.iface_cashdrawer) {

                this.pos.proxy.open_cashbox();
        }

        order.initialize_validation_date();

        if (order.is_to_invoice()) {
            var invoiced = this.pos.push_and_invoice_order(order);
            this.invoicing = true;

            invoiced.fail(function(error){
                self.invoicing = false;
                if (error.message === 'Missing Customer') {
                    self.gui.show_popup('confirm',{
                        'title': _t('Please select the Customer'),
                        'body': _t('You need to select the customer before you can invoice an order.'),
                        confirm: function(){
                            self.gui.show_screen('clientlist');
                        },
                    });
                } else if (error.code < 0) {        // XmlHttpRequest Errors
                    self.gui.show_popup('error',{
                        'title': _t('The order could not be sent'),
                        'body': _t('Check your internet connection and try again.'),
                    });
                } else if (error.code === 200) {    // OpenERP Server Errors
                    self.gui.show_popup('error-traceback',{
                        'title': error.data.message || _t("Server Error"),
                        'body': error.data.debug || _t('The server encountered an error while receiving your order.'),
                    });
                } else {                            // ???
                    self.gui.show_popup('error',{
                        'title': _t("Unknown Error"),
                        'body':  _t("The order could not be sent to the server due to an unknown error"),
                    });
                }
            });

            invoiced.done(function(){
                self.invoicing = false;
                self.gui.show_screen('receipt');
            });
        } else {
            if (order.get_livraison()){
                var livreur = self.$( "#selection_livreur option:selected" ).text();
                if (livreur && livreur !== "Selectionner livreur"){
                    this.pos.push_order(order);
                    this.gui.show_screen('receipt');
                }
                else{
                    this.pos.push_order(order);
                    this.gui.show_screen('receipt');
                }
            }else{
                this.pos.push_order(order);
                this.gui.show_screen('receipt');
            }
        }
    },
    });


});
