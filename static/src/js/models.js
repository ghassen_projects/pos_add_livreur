odoo.define('pos_add_livreur.models', function (require) {
"use strict";

    var module = require('point_of_sale.models');
    var models = module.PosModel.prototype.models;


    module.load_models({
        model:  'hr.employee',
        fields: ['name','country_id','birthday','job_id','work_phone','city','is_employee','is_livreur'],
        domain: [['is_livreur','=',true]],
        loaded: function(self,livreurs){
            self.livreurs = livreurs;
            self.livreurs_by_id = {};
                for (var i = 0; i < livreurs.length; i++) {
                    self.livreurs_by_id[livreurs[i].id] = livreurs[i];
                }
            },
    });
    var _super_order = module.Order.prototype;
       module.Order = module.Order.extend({

        initialize: function(){
            _super_order.initialize.apply(this,arguments);

            this.livraison = 0;
            this.domicile = 0;
            this.livreurs = this.pos.livreurs;
            this.selected_livreur;
            this.click;

            this.save_to_db();

        },
        init_from_JSON: function(json) {
        _super_order.init_from_JSON.apply(this,arguments);
        this.livreur = this.pos.livreurs_by_id[json.livreur_id];
        },
        export_as_JSON: function() {
        var json = _super_order.export_as_JSON.apply(this,arguments);
        json.livraison =   this.get_livraison();
        json.domicile  =   this.get_domicile();
        json.livreur_id=   this.get_selected_livreur() ? this.get_selected_livreur().id : false;
        return json;
        },
        set_livraison: function(livraison) {
            this.livraison = livraison;
            this.trigger('change');
        },

        get_livraison: function(){
                return this.livraison;
        },
        set_domicile: function(domicile){
            this.domicile = domicile;
            this.trigger('change');
        },
        get_domicile: function(){
            return this.domicile;
        },
        get_livreurs: function(){
            var livreurs_in_pos = [];
            console.log('get_livreurs');
            for(var i=0; i < this.livreurs.length ; i++){
                livreurs_in_pos.push(this.livreurs[i]);
            }
            return livreurs_in_pos;
        },
        set_selected_livreur: function(livreur){
            this.selected_livreur = livreur;
            this.trigger('change');
        },
        get_selected_livreur: function(){
            console.log('selected_livreur = '+this.selected_livreur);
            return this.selected_livreur;
        },
        set_click: function(c){
            this.click = c;
        },
        get_click: function(){
            return this.click;
        }
	});

});